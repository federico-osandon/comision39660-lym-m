import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import { BrowserRouter } from 'react-router-dom'
// import './index.css'

// ( <div id="root"> {{ App() }} </div>   )
ReactDOM.createRoot(document.getElementById('root')).render(<App />)
