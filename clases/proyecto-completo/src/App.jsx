import { Navigate, Route, BrowserRouter as Router, Routes } from 'react-router-dom'

import { NavBar } from './components/NavBar/NavBar'
import ItemListContainer from './components/ItemListContainer/ItemListContainer'
import { ItemDetailContanier } from './components/ItemDetailContainer/ItemDetailContanier'
import { CartContainer } from './components/CartContainer/CartContainer'


import 'bootstrap/dist/css/bootstrap.min.css'
import { CartContextProvider } from './context/CartContext'
import { TextComponent } from '../comision39660-lym-m/clases/clase12-rendering/ComponenteEjemplosCondicionales'




function App() {   
    // estado y funciones globales

    return ( 
        <CartContextProvider>
            <div 
                // className="border border-5 border-warning  m-3" 
                // onClick={()=>alert('Soy evento de APP')}
            >
                <Router>
                    
                    <NavBar />   
                    <Routes>
                        <Route 
                            path='/' 
                            element={<ItemListContainer />} 
                        />
                        <Route 
                            path='/categoria/:categoria/' 
                            element={<ItemListContainer />} 

                        />

                        <Route 
                            path='/detail/:pid/' 
                            element={<ItemDetailContanier />}
                        />
                        {/* CartContainer es solo ejemplo no entra en el desafío */}
                        <Route 
                            path='/cart' 
                            element={<CartContainer />} 

                        />  
                        {/* <Route 
                            path='/formProduct' 
                            element={
                                <TextComponent>
                                    <FormIngProducto />
                                </TextComponent>
                            } 
                        />   */}

                        {/* <Route path='/notfound' element={<NotFound404 />} />       */}

                        {/* <Route path='*' element={ <Navigate to='/notfound' /> } />             */}
                        <Route path='*' element={ <Navigate to='/' /> } />            
                    </Routes>
                    {/* <ItemCount /> */}
                    {/* <Footer /> */}
                </Router>            
            </div>
    
        </CartContextProvider>
    )
}

export default App
