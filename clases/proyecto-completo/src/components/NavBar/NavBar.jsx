import { Link, NavLink, useParams } from "react-router-dom"
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap"
import CartWidget from "../CartWiget/CartWidget"

export const NavBar = () => { 
      
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>
                <Link to='/' className='alert alert-success' >
                    Fede-Ecommerce
                </Link>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="me-auto">
                    <NavLink to="/categoria/gorras" className={ ({isActive})=> isActive ? 'btn btn-primary' : 'btn btn-outline-primary'} >Gorras</NavLink>
                    <NavLink to='/categoria/remeras' className={ ({isActive})=> isActive ? 'btn btn-primary' : 'btn btn-outline-primary'}>Remeras</NavLink>                   
                </Nav>
                <Nav>                    
                    <Link className="" to='/cart' >
                        <CartWidget />
                    </Link>
                    
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}


