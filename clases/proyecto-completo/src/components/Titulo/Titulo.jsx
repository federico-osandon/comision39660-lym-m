
const Titulo = ( { titulo, subTitulo } ) => {
    // console.log(props)
    return (
        <>
            <h1 className='App'>Esto es un titulo de {titulo}</h1>
            <h2 className='App'>Subtititulo de {subTitulo}</h2>
        </>
    )
}

export default Titulo