import {  useState } from "react"

import { useCartContext } from "../../context/CartContext"

import ItemCount from "../ItemCount/ItemCount"
import { Link } from "react-router-dom"

export const ItemDetail = ({producto}) => {
    const [ isInCant, setIsInCant ] = useState(false)

    const {addToCart} = useCartContext()

    const onAdd = (cantidad)=>{
        console.log(cantidad)
        addToCart( { ...producto, cantidad } )
        setIsInCant(true)
    }   
    console.log(producto)
    return (
        <>
            <div className="row">
                <div className="col">
                    <img src={producto?.foto} className="w-50" alt="imagen" />
                    <h3>Nombre: {producto.name}</h3>
                    <h3>Categoría: {producto.categoria}</h3>
                    <h3>Precio: {producto.precio}</h3>
                    <h3>Stock: {producto.stock}</h3>
                </div>
                <div className="col">
                    {isInCant  ? 
                            <>
                                <Link className="btn btn-outline-primary" to='/cart'>Terminar Compra</Link>
                                <Link className="btn btn-outline-primary" to='/'>Continuar Compra</Link>
                            </>
                        : 
                            <ItemCount onAdd={onAdd}/>
                    }

                    {/* <>
                        {!isInCant && <ItemCount onAdd={onAdd} /> }
                        {isInCant &&  <Link to='/cart'>Terminar Compra</Link> }
                    </> */}
                  
                      

                </div>

            </div>

        </>
    )
}
