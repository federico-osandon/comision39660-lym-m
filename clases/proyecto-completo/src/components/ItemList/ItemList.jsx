import { memo } from "react"
import { Link } from "react-router-dom"
import Item from "../Item/Item"

// memo() de react -> 1- memo(componente) -> memo(componente, fn comparadora)

const ItemList = memo( ({productos}) => {
    
        console.log('ItemList')
        return (
            <div style={{
                display: "flex",
                flexDirection: 'row',
                flexWrap: "wrap"
            }}>
                {/* <Filter >
                    { handleProductFiltered }
                </Filter> */}
                {productos.map( ({id ,foto, name, price, categoria}) =>  (
                    <Item 
                        key={id} 
                        id={id}
                        foto={foto} 
                        name={name} 
                        price={price}
                        categoria={categoria}
                    />
                ))}
                
               
            </div>
        )
    }
) 
// , (prevProps, nextProps)=> prevProps.productos === nextProps.productos) 

export default ItemList