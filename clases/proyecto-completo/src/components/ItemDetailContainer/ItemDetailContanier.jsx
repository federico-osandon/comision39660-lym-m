import { useParams } from "react-router-dom"
import { ItemDetail } from "../ItemDetail/ItemDetail"
import { useEffect, useState } from "react"
import { mFetch } from "../../utils/mFetch"
import { TextComponent, TextComponent2, TextComponent3, TextComponent4, TextComponent5, TextComponent6, TextComponent7 } from "../../../comision39660-lym-m/clases/clase12-rendering/ComponenteEjemplosCondicionales"

export const ItemDetailContanier = () => {
    const [producto, setProducto] = useState({})
    const [isLoading, setIsLoading] = useState(true)
    const { pid } = useParams() // pid

    useEffect(()=>{ // efecto secundarío 

        mFetch(pid)
        .then(resp => setProducto(resp))
        .catch(err => console.log(err))
        .finally(()=> setIsLoading(false))

    }, [])
    

    console.log(pid)
    return (
        <div 
            // className="border border-5 border-primary  m-3" 
        >
            {isLoading ? 
                <h2>Cargando...</h2>
            :
                <>
                    {/* <TextComponent >
                    </TextComponent> */}
                    <ItemDetail producto={producto} />
                    {/* <TextComponent2 /> */}
                    {/* <TextComponent3 /> */}
                    {/* <TextComponent4 /> */}
                    {/* <TextComponent5 /> */}
                    {/* <TextComponent7  /> */}
                </>
            }            
        </div>
    )
}
