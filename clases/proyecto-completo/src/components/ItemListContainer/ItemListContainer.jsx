import { useEffect, useState } from "react"
import {  useParams } from "react-router-dom"
import { collection, doc, getDoc, getDocs, getFirestore, limit, orderBy, query, where } from 'firebase/firestore'
import { mFetch } from "../../utils/mFetch"
import ItemList from "../ItemList/ItemList"
import { Loading } from "../Loading/Loading"



const ItemListContainer = ({ greeting }) => {
    const [productos, setProductos] = useState([])
    const [producto, setProducto] = useState({})
    const [isLoading, setIsLoading] = useState(true)
    const [meGusta, setMeGusta] = useState(true)
    // if, for, {} estado - useEffect


    const { categoria } = useParams()

    // const [productos, loading] = useFetch()
    useEffect(()=>{
        const dbFirestore     = getFirestore()
        const queryCollection = collection(dbFirestore, 'productos')

        if (!categoria) {  
            getDocs(queryCollection)
                .then(res => setProductos(  res.docs.map(producto => ( { id: producto.id, ...producto.data() } )) ))
                .catch( error => console.log(error) )
                .finally(()=> setIsLoading(false))
        }else{
            const queryCollectionFiltered = query(
                queryCollection, 
                where('categoria','==', categoria),
                // orderBy('price', 'asc'),
                // limit(1)
            )
    
            getDocs(queryCollectionFiltered)
                .then(res => setProductos(  res.docs.map(producto => ( { id: producto.id, ...producto.data() } )) ))
                .catch( error => console.log(error) )
                .finally(()=> setIsLoading(false))
        }
    }, [categoria])

    
    // traer un producto(documento) -> ItemDetailContainer - id =>(pid)
    // useEffect(()=>{
    //     const dbFirestore = getFirestore()
    //     const queryDoc = doc(dbFirestore, 'productos', '4GlOn8NgTU3V8s16NTgr')

    //     getDoc(queryDoc) // -> getDocs  ()=>{}  { id: resp.id, ...resp.data() }
    //         .then( resp => setProducto( { id: resp.id, ...resp.data() } ) )
    //         .catch(err => console.log(err))

    // },[])

    // traer todos los productos
    // useEffect(()=> {
    //     const dbFirestore     = getFirestore()
    //     const queryCollection = collection(dbFirestore, 'productos')

    //     getDocs(queryCollection)
    //         .then(res => setProductos(  res.docs.map(producto => ( { id: producto.id, ...producto.data() } )) ))
    //         .catch( error => console.log(error) )
    //         .finally(()=> setIsLoading(false))
    // }, [])

    // traer productos filtrados 

    // useEffect(()=> {
    //     const dbFirestore     = getFirestore()
    //     const queryCollection = collection(dbFirestore, 'productos')

    //     const queryCollectionFiltered = query(
    //         queryCollection, 
    //         where('price','>=', 1500),
    //         // orderBy('price', 'asc'),
    //         // limit(1)
    //     )

    //     getDocs(queryCollectionFiltered)
    //         .then(res => setProductos(  res.docs.map(producto => ( { id: producto.id, ...producto.data() } )) ))
    //         .catch( error => console.log(error) )
    //         .finally(()=> setIsLoading(false))
    // }, [])


    
    // dos funciones

        // simular un me gusta
        const handleMeGusta = ()=> {
            setMeGusta(!meGusta)
        } 
        
        // simular agregar un producto
        const handleAgregarUnProducto = ()=> {
            setProductos( [
                ...productos, 
                {id: productos.length +1 , name: 'Prodcuto de prueba', price: 1500, categoria: 'remeras', foto: 'https://th.bing.com/th/id/R.ec9766c1703a81d16efdf593ba739358?rik=Y1%2bUij7Q9ajrGQ&pid=ImgRaw&r=0'}
            ] )
        } 
        

    console.log(productos)
    return (
        <>
            <button onClick={handleMeGusta}>Me gusta</button>
            <button onClick={handleAgregarUnProducto}>Agregar Productos</button>
            {isLoading ? 
                    <Loading />
                :
                    <ItemList productos={productos} />
            }
        </>
        
    )
}

export default ItemListContainer


// console.log(categoria)
// const handleProductFiltered = ({ filterState, handleFilterChange }) => (
//     <center>
//         <h2>Buscar  Producto</h2>
//         <br></br>
//         {/* {filterState} */}
//         <input type="text" value={filterState} onChange={handleFilterChange} />

//         { isLoading ?
//                 <h2>Cargando...</h2>
//             : 
//                 <>
//                     {filterState === '' 
                    
//                         ? productos.map( ({id, foto, name, price, categoria}) =>  <div key={id} className="card w-25">
//                                                         <img src={foto} className="card-img-top" alt="imagen-card" />
//                                                         <div className="card-body">
//                                                             <h6>Nombre: {name}</h6>
//                                                             <label>Precio: {price}</label>
//                                                             <label>Categoria: {categoria}</label>
//                                                         </div>
//                                                         <div className="card-footer">
//                                                             <button className="btn btn-outline-dark">Detalle</button>
//                                                         </div>
//                                                     </div>
//                         )
//                         : 
//                         productos.filter( producto => producto.name.toLowerCase().includes(filterState.toLowerCase()) ).map( ({id, foto, name, price, categoria}) =>  <div key={id} className="card w-25">
//                                                         <img src={foto} className="card-img-top" alt="imagen-card" />
//                                                         <div className="card-body">
//                                                             <h6>Nombre: {name}</h6>
//                                                             <label>Precio: {price}</label>
//                                                             <label>Categoria: {categoria}</label>
//                                                         </div>
//                                                         <Link to='/detail'>
//                                                             <button className="btn btn-outline-dark">Detalle</button>
//                                                         </Link>
                                                        
//                                                     </div>
//                         )
//                     } 

//                 </>
//         }
//     </center>
// )
