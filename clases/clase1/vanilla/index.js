// document.querySelector('#root')
let componente = document.getElementById('root')
console.log(componente)

const titulo = ()=>{
    const titulo = document.createElement('h1')
    titulo.id = 'titulo'

    const parrafo = document.createElement('p')
    parrafo.id = 'parrafo'

    // agregar texto a los elementos
    titulo.textContent = 'My First Heading'
    parrafo.textContent = 'My first paragraph.'

    //agregar elementos a los node del Dom
    componente.appendChild(titulo)
    componente.appendChild(parrafo)
}

const app = function () { // ??
    // nav()
    titulo()
    // footer()
}

app()

// varela Riber