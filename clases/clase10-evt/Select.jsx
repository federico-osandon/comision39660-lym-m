export default function Select({ options, optionSelected, option }) {
    return (
        <>
            {option===1 ? 
                <img src='https://static.dafiti.com.ar/p/tommy-hilfiger-6711-936993-1-product.jpg' alt='foto' className="w-25" />
                : 
                <img src='https://static.dafiti.com.ar/p/everlast-0575-351435-1-product.jpg' alt='foto' className="w-25" /> 
            }
            <select 
                onChange={(evt) => optionSelected(Number(evt.target.value))}
            >            
                {
                    options.map((val) => (
                        <option value={val.value}>{val.text}</option>
                    ))
                }

            </select>   

            {/* {options.map(item => (
                <>
                    <input 
                        onChange={(event)=>{
                            optionSelected(item.value)
                        }}
                        type="radio"
                        name='radio'
                        checked={option===item.value}
                        id={item.value}
                    />
                    <label for={item.value}>{item.text}</label>
                </>            
            ))} */}



        </>

    )
}
  