import {useState, useEffect} from 'react'
// import './input.css'


export const Input = () => {   

    const inputHandler = (event)=>{
        if (['a','e','i','o','u'].includes(event.key.toLowerCase())) {
            event.preventDefault()  
            
        } 
        // event.stopPropagation()    
        // estados  para guardar valores
        console.log(event.key)          
          
    }
    
    return (
        <div className="box" >
            <div className="border border-5 border-warning  m-3" 
                // onClick={() => alert('evento del div de Input')}
            >
                <input 
                    className="m-5 " 
                    onKeyDown={ inputHandler } // con pascalCase
                    // onClick = {  inputHandler } 
                    type="text" 
                    name="nombre" 
                    id="i"
                />
            </div>
        </div>
    )
}
