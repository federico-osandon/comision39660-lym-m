import './App.css'


let condition = false
// let resultado = ''

// if (condition) {
//   resultado = 'Verdadero'
// } else {
//   resultado = 'Falso'  
// }



// console.log('El resultado es: \n' + resultado + '')
// console.log(`El resultado es: ${ condition ? 'Verdado': 'Falso' }`)

// spread operator

// let array = [  1,2,3  ]
// let numeroCuatro = 4

// let nuevoArray = [ ...array, numeroCuatro ]

// Propiedades dinámicas.

// let email= 'fe@gmail.com'
// let campo = 'id'

// let objetoPersona = {
//   nombre: 'Fede',
//   apellido: 'Osandón',
//   [ campo ]: email,
//   dni: 11111111
// }

// // deep Matching
//     // destructuración
//     // let nombre = objetoPersona.nombre 
//     // let apellido = objetoPersona.apellido 
//     const { nombre: first_name, apellido, dni=2222222  } = objetoPersona




// console.log(first_name, apellido, dni)

// Ejercicio

// function findPolyfi(array, callback) {
//   for (let i = 0; i < array.length; i++) {
//     if (callback(array[i], i, array)) {
//       return array[i]
//     }
//   }
// }

// let fruits = ['apple', 'banana', 'orange', 'pear']

// console.log(findPolyfi(fruits, (fruit) => {
//   return fruit === 'orange';
// }))



function App() {


  return (
    <div className="App">
      App
    </div>
  )
}

export default App
