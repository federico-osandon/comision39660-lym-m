import { addDoc, collection, doc, getFirestore, updateDoc, writeBatch } from "firebase/firestore"
import { useCartContext } from "../../context/CartContext"
import { useState } from "react"

export const CartContainer = () => {
    const [dataForm, setDataForm] = useState({
        name: '',
        phone: '',
        email: ''
    })
    const {cartList,  vaciarCarrtio, precioTotal} = useCartContext()

    const generarOrden = (evt)=>{
        evt.preventDefault()

        const order = {}
        order.buyer = dataForm
        order.items = cartList.map(({name, id, price, cantidad}) => ({id, name, price, cantidad}))
        order.total = precioTotal()
        // console.log(order)

        // insertar la orden a firebase -> firestore(productos, etc..)
        const dbFirestore = getFirestore()
        const ordersCollection = collection(dbFirestore, 'orders')

        addDoc(ordersCollection, order)
        .then(resp => console.log(resp))


        // actualizar update
        
        // const queryDoc = doc(dbFirestore, 'productos', 'oF5JEM8kPSGRoB2Ucz1i')
        // const queryDoc2 = doc(dbFirestore, 'productos', '4GlOn8NgTU3V8s16NTgr')
        

        // updateDoc(queryDoc, {
        //     stock: 0
        // })
        // .finally(() => console.log('finalizó la actualización'))

        // borrado lógico
        // updateDoc(queryDoc, {
        //     isActive: false
        // })
        // .finally(() => console.log('finalizó la actualización'))

        // const batch = writeBatch(dbFirestore)
        // batch.update(queryDoc, {
        //     stock: 150
        // })
        // batch.update(queryDoc2, {
        //     stock: 100
        // })
        // // batch.set()

        // batch.commit()

        
    }

    const handleOnChange = (evt)=>{
        console.log('nombre del input',evt.target.name)
        console.log('valor del input',evt.target.value)
        setDataForm({
            ...dataForm,
            [evt.target.name]: evt.target.value
        })
    }

    // para insertar muchos productos
    // arrayProductos.forEach(async element => {
    //     addDoc(collection, producto)
    // })

    // 4 input 1 validar el mail
    console.log(dataForm)
    return (
        <div>
            {cartList.map(prod => (
                <div className="w-50">
                    <img className="w-25" src={prod.foto} alt="imagen"/>
                    <label> Precio {prod.price} - Cantidad : {prod.cantidad}</label>
                    <button > X </button>
                </div>
            ))}
            <button onClick={vaciarCarrtio} className="btn btn-outline-danger">Vaciar Carrito</button>

            <form onSubmit={generarOrden}> 
                <input 
                    type='text' 
                    name="name" 
                    onChange={handleOnChange}
                    value={dataForm.name} 
                    placeholder="ingrese el nombre" 
                /> 
                <input 
                    type='text' 
                    name="phone" 
                    onChange={handleOnChange}
                    value={dataForm.phone} 
                    placeholder="ingrese el tel " 
                /> 
                <input 
                    type='text' 
                    name="email" 
                    onChange={handleOnChange}
                    value={dataForm.email} 
                    placeholder="ingrese el email" 
                /> 

                <button className="btn btn-outline-danger">generar orden</button>
            </form>

        </div>
    )
}
