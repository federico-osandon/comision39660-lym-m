import { useEffect, useRef, useState } from 'react'
import './App.css'
import { Menu } from './components/Menu/Menu'
import Titulo from './components/Titulo/Titulo'
import ItemCount from './components/ItemCount/ItemCount'

// Menu()
function App() {   
    const [booleano, setBooleano] = useState(true)
    const [contenido, setContenido] = useState('Actual')
    
    const divRef = useRef(null)   
    
    useEffect(()=>{
        console.log('window.addEventListener("evento", function)')

        return () => {
            console.log('efecto de limpieza- window.removeEventListener("evento", function)')
        }
    })

    useEffect(()=>{
        // una sola vez al principio, en el mount
        console.log('llamada a las api - Mock(simulación) - fetch - 2')
    }, [])

    useEffect(()=>{
        
        console.log('Cada vez que cambie algo del array de dependencia- 3')
    }, [contenido, booleano])

    let handleClick = () => {        
        divRef.current.innerText = 'Nuevo contenidos' 
        setContenido('Nuevo')
    }
    let handleBooleano = () => {        
        setBooleano(!booleano)
    }



    // console.log('Renderizado de app - 4')
    return ( 
        <div  >
            
            <Menu />
            <button onClick={handleBooleano}>Me Gusta</button>
            
            <ItemCount />
            
            <div ref={divRef}>Contenido Actual</div>
            <button onClick={handleClick}>Cambiar el contenido</button>
            
            <hr/>
        </div>
    )
}

export default App

// el primer render se llama montaje
// re render:
    // 1-  Un evento
    // 2-  Cambio de estado
    // 3-  Un cambio en las props

